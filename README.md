# YOLO-NAS and Segement Anything Model(SAM)

The YOLO NAS (You Only Look Once Neural Architecture Search) model is an innovative approach to automated architecture search in the field of object detection. Object detection plays a crucial role in computer vision tasks, and YOLO has emerged as a popular framework for real-time object detection. However, manually designing architectures that balance accuracy and efficiency is a challenging and time-consuming process. The YOLO NAS model addresses this problem by employing neural architecture search techniques to automatically discover high-performance object detection architectures.

YOLO NAS model represents a significant advancement in object detection through automated architecture search. By combining the strengths of YOLO and NAS, this model offers a powerful tool for real-time object detection tasks, paving the way for improved accuracy and efficiency in computer vision applications.

The new YOLO-NAS delivers state-of-the-art performance with the unparalleled accuracy-speed performance, outperforming other models such as YOLOv5, YOLOv6, YOLOv7 and YOLOv8.

![alt text](https://github.com/Deci-AI/super-gradients/blob/master/documentation/source/images/yolo_nas_frontier.png?raw=true)

SAM is an efficient and promptable model for image segmentation. With over 1 billion masks on 11M licensed and privacy-respecting images, SAM’s zero-shot performance is often competitive with or even superior to prior fully supervised results. 

The SAM model can be loaded with 3 different encoders: ViT-B, ViT-L, and ViT-H. ViT-H improves substantially over ViT-B but has only marginal gains over ViT-L. These encoders have different parameter counts, with ViT-B having 91M, ViT-L having 308M, and ViT-H having 636M parameters. This difference in size also influences the speed of inference, so keep that in mind when choosing the encoder for your specific use case.

![alt text](https://segments.ai/assets/img/segments/hero/segmentation.png)
